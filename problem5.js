// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const inventory=require('./cars');
const problem4 = require('./problem4');


function problem5(inventory) {
    const years = problem4(inventory);
    let olderCars = new Array();
    for (let index = 0; index < years.length; index++) {
        if (years[index] < 2000) {
            olderCars.push(years[index]);
        }
    }
    return olderCars;
}

module.exports = problem5;
