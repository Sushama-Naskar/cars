// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortName(name) {
    
    for (let selectedIndex = 0; selectedIndex < name.length; selectedIndex++) {
        
        for (let currentIndex = selectedIndex + 1; currentIndex < name.length; currentIndex++) {
            if (name[currentIndex].car_model.toLowerCase() < name[selectedIndex].car_model.toLowerCase()) {
                
                let temp = name[currentIndex];
                name[currentIndex] = name[selectedIndex];
                name[selectedIndex] = temp;

            }
        }
    }
}

function problem3(inventory) {
    let name = inventory;
    sortName(name);
    return name;
}

module.exports = problem3;

